package com.example.admin.testassesmentpuna;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by admin on 02-07-2018.
 */

public class CustomAdapter extends BaseAdapter {
    private static LayoutInflater inflater=null;
    private Activity activity;
    private ArrayList<HashMap<String, String>> data;

    public CustomAdapter(Activity a, ArrayList<HashMap<String, String>> d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View vi=view;
        if(view==null)
            vi = inflater.inflate(R.layout.list_row, null);

        TextView fileName = (TextView)vi.findViewById(R.id.title); // title
        TextView lastModifed = (TextView)vi.findViewById(R.id.duration); // artist name


        HashMap<String, String> files = new HashMap<String, String>();
        files = data.get(i);

        // Setting all values in listview
        fileName.setText(files.get(DownloadedFileList.KEY_FILENAME));
        lastModifed.setText(files.get(DownloadedFileList.KEY_LASTMODIFIED));

        return vi;
    }
}
