package com.example.admin.testassesmentpuna;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;

import javax.security.auth.login.LoginException;

public class DownloadedFileList extends AppCompatActivity {
    ListView listView ;
    TextView noFiles;
    CustomAdapter adapter;
    static final String KEY_FILENAME = "fileName";
    static final String KEY_LASTMODIFIED = "dateTime";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloaded_file_list);

        listView = (ListView) findViewById(R.id.list);
        noFiles = (TextView) findViewById(R.id.noFiles);
        String path = Environment.getExternalStorageDirectory().toString()+"/Download/Test";;
        Log.e("Downloaded PAth", "onCreate: "+path);
        File directory = new File(path);

        if(directory.exists()){


        ArrayList<HashMap<String, String>> fileList = new ArrayList<HashMap<String, String>>();
        File[] files = directory.listFiles();
        Arrays.sort(files, new Comparator<File>(){
            public int compare(File f1, File f2) {
                return Long.valueOf(f2.lastModified()).compareTo(f1.lastModified());
            }
        });
        Log.e("Files", "Size: "+ files.length);
        Log.e("Files", "list of file: "+ directory.list());
        for (int i = 0; i < files.length; i++)
        {   Date lastModDate = new Date(files[i].lastModified());

            HashMap<String, String> map = new HashMap<String, String>();
            map.put(KEY_FILENAME, files[i].getName());
            map.put(KEY_LASTMODIFIED,new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a").format(
                    new Date(files[i].lastModified())
            ));

            fileList.add(map);
            Log.e("Files", "FileName:" + files[i].getName() + " TIme: " + new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(
                    new Date(files[i].lastModified())
            ));
        }
        adapter=new CustomAdapter(this, fileList);
        listView.setAdapter(adapter);
    }
    else {
            listView.setVisibility(View.GONE);
            noFiles.setVisibility(View.VISIBLE);
            noFiles.setText("No Downloads");
        }
    }
}
