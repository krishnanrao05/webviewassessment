package com.example.admin.testassesmentpuna;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ParseException;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.DownloadListener;
import android.webkit.URLUtil;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.util.StringTokenizer;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText urlText;
    Button submitButton,retrieveButton;
    WebView webView;
    private Activity mActivity;
    private static final int MY_PERMISSION_REQUEST_CODE = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        webView = (WebView)findViewById(R.id.webView);
        urlText = (EditText)findViewById(R.id.urlToDownload);
        submitButton = (Button)findViewById(R.id.submitBtn);
        retrieveButton = (Button)findViewById(R.id.retrieveButton);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        urlText.setText("https://drive.google.com/drive/folders/17GKe5kK5ESOMuJqeLmaoVN03YpBAmbUE?usp=sharing");
        urlText.setSelection(urlText.getText().length());
        submitButton.setOnClickListener(this);
        retrieveButton.setOnClickListener(this);

        mActivity = this;

    }
    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.submitBtn){
boolean isconnected = ConnectivityReceiver.isConnected(this);
            if(isconnected) {
                final ProgressDialog pd = new ProgressDialog(this);
                pd.setTitle("Loading....");
                pd.setCancelable(false);
                pd.show();
                WebSettings webSettings = webView.getSettings();
                webSettings.setJavaScriptEnabled(true);
                webSettings.setUseWideViewPort(true);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    webSettings.setAllowFileAccessFromFileURLs(true);
                }
                webView.setWebViewClient(new WebViewClient() {
                    public void onPageFinished(WebView view, String url) {
                        pd.dismiss();

                    }
                });

                webView.loadUrl(urlText.getText().toString());

                webView.setDownloadListener(new DownloadListener() {
                    @Override
                    public void onDownloadStart(String url, String userAgent, String contentDescription, String mimetype, long contentLength) {
                        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                        request.allowScanningByMediaScanner();
                        request.setNotificationVisibility(
                                DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                        String fileName = URLUtil.guessFileName(url, contentDescription, mimetype);
                        StringTokenizer content = new StringTokenizer(contentDescription, ";");
                        String s1 = content.nextToken();
                        String s2 = content.nextToken();
                        String s3 = content.nextToken();
                        StringTokenizer contentFilename = new StringTokenizer(s2, "=");
                        String optionName = contentFilename.nextToken();
                        String fileNameDetails = contentFilename.nextToken();
                        StringTokenizer extension = new StringTokenizer(fileNameDetails.replaceAll("^\"|\"$", ""), ".");
                        String fName = extension.nextToken();
                        String estensionName = extension.nextToken();

                        Log.e(" Download File: ", " onDownloadStart: fileName: " + fileName + "mimetype " + mimetype + " contentDescription " + contentDescription);
                        Log.e(" Download File: ", " onDownloadStart: Environment.DIRECTORY_DOWNLOADS: " + Environment.DIRECTORY_DOWNLOADS + " s1 " + s1 + " s2 " + s2 + " s3 " + s3);
                        Log.e(" Download File: ", " onDownloadStart: fileNameDetails " + fileNameDetails.replaceAll("^\"|\"$", ""));
                        Log.e(" Download File: ", " onDownloadStart: estensionName " + estensionName + " fName " + fName);
                        if (estensionName.equalsIgnoreCase("mp3") || estensionName.equalsIgnoreCase("mp4")) {
                            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS + "/Test/", fileNameDetails.replaceAll("^\"|\"$", ""));
                            DownloadManager dManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);

                            dManager.enqueue(request);

                        } else {
                            Toast.makeText(mActivity, "Cannot Download Except MP3 and MP4 : ", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
            }
            else {
                Toast.makeText(mActivity, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }
        if(view.getId() == R.id.retrieveButton){
            Intent ref = new Intent(MainActivity.this,DownloadedFileList.class);
            startActivity(ref);
        }
    }

    protected void checkPermission(){
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
            if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
                if(shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                    // show an alert dialog
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage("Write external storage permission is required.");
                    builder.setTitle("Please grant permission");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ActivityCompat.requestPermissions(
                                    mActivity,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    MY_PERMISSION_REQUEST_CODE
                            );
                        }
                    });
                    builder.setNeutralButton("Cancel",null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }else {
                    // Request permission
                    ActivityCompat.requestPermissions(
                            mActivity,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSION_REQUEST_CODE
                    );
                }
            }else {
                // Permission already granted
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        switch(requestCode){
            case MY_PERMISSION_REQUEST_CODE:{
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    // Permission granted
                }else {
                    // Permission denied
                }
            }
        }
    }

    @Override
    public void onBackPressed(){
        if(webView.canGoBack()){
            // If web view have back history, then go to the web view back history
            webView.goBack();

            Toast.makeText(mActivity, "back press", Toast.LENGTH_SHORT).show();
        }else {
            // Ask the user to exit the app or stay in here
            showAppExitDialog();
        }
    }

    protected void showAppExitDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

        builder.setTitle("Please confirm");
        builder.setMessage("No back history found, want to exit the app?");
        builder.setCancelable(true);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Do something when user want to exit the app
                // Let allow the system to handle the event, such as exit the app
                MainActivity.super.onBackPressed();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Do something when want to stay in the app
               // Toast.makeText(mActivity,"thank you",Toast.LENGTH_LONG).show();
            }
        });

        // Create the alert dialog using alert dialog builder
        AlertDialog dialog = builder.create();

        // Finally, display the dialog when user press back button
        dialog.show();
    }
}
